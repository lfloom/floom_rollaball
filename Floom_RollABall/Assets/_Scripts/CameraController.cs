﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //variables 
    public GameObject player;
    private Vector3 offset; 

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position; 
    }

    // Update is called once per frame
    void LateUpdate() //called at the very end of all the update loops (used for camera positioning) 
    {
        transform.position = player.transform.position + offset; //finds offset and then places the camera there 
    }
}
