﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System; 

public class Bumpers : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //prints out the key commands for the user in the consol on how to control the bumpers 
        print("click the space bar to get rid of bumpers!");
        print("click the backspace button to put bumpers on!");
    }

    // Update is called once per frame
    void Update()
    {

        if ( Input.GetKeyDown(KeyCode.Space)) //when the space bar is pressed, the bumpers go down
        {

           // gameObject.SetActive(false); 
           
            transform.localScale = new Vector3(0, 0, 0);  

        }

        if(Input.GetKeyDown(KeyCode.Backspace)) //when the backspace key is pressed, the bumpers go up  
        {
           // gameObject.SetActive(true);
            transform.localScale = new Vector3(.5f, 2, 20.5f); 
        }
    }

}
