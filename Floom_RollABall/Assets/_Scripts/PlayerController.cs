﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; 

public class PlayerController : MonoBehaviour
{
    //variables (null before initiated)
    //variables in C# are private automatically so you technically don't need to make them private 
    public float speed;
    private Rigidbody rb; //private
   


    // Start is called before the first frame update
    void Start() //like a constructor 
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical); //not moving on the y axis because that would be flying 

        rb.AddForce(movement * speed); //gets vecotr input and multiplies it times speed to make it move 
    }

    //method for when the ball and the pickup pieces collide
    private void OnTriggerEnter(Collider other)
    {
        // Object array of all the pins (pickups)
        GameObject[] allPins = GameObject.FindGameObjectsWithTag("Pick Up");
        //GameObject scoreWall = GameObject.FindGameObjectWithTag("scoreWall");
       // scoreWall.SetActive(false); 
        //String strike = "Assets/Resources/Materials/Strike";
        //bool runPush = true; //boolean to run pin pusher 

        //variable for position ball must be at to get a strike
        Vector3 strikePosnLower = new Vector3(-.04f, 1.55f, 5.98f); //lower bound
        Vector3 strikePosnUpper = new Vector3(1.96f, 1.55f, 5.98f); //upper bound
        if (other.gameObject.CompareTag("One") && gameObject.transform.position.x >= strikePosnLower.x && gameObject.transform.position.x <= strikePosnUpper.x)
        {
            //other.gameObject.SetActive(false); 
            other.transform.localRotation = Quaternion.Euler(90, 180, 0); //changes rotation of the pins so they are on the ground
            other.transform.localPosition -= new Vector3(0, 1.1f, 0); //changes position of pins so they are on the ground

            for (int x = 0; x < allPins.Length; x++)
            {
                //allPins[x].SetActive(false);
                allPins[x].transform.localRotation = Quaternion.Euler(90, 180, 0); //changes rotation of all other pins so they are on the ground
                allPins[x].transform.localPosition -= new Vector3(0, 1.1f, 0); //changes postion of all other pins so they are on the ground
            }
            gameObject.SetActive(false);
            //scoreWall.SetActive(true); 


        }
        else
        {
            if (other.gameObject.CompareTag("Pick Up")|| other.gameObject.CompareTag("One")) //if a pin or the main pin in front is not hit at the right angle for a strike...
            {
                other.gameObject.SetActive(false); //pins dissapear when hit
            }

            if(gameObject.transform.position.z > 8) //if the ball goes past the threshold of the back pins...
            {
                gameObject.transform.localPosition = new Vector3(-.3f, .5f, -3.7f); //the ball is set back to its starting positon 

                    }
            }

        
    }
}
